// dated.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

typedef unsigned char uint8;
typedef unsigned int uint32;

// ----------------------------------------------------------------------------
// -- Developer Options
#ifdef _DEBUG
#define DUMP_HEADER_ERROR
#endif
// ----------------------------------------------------------------------------
// -- LMXBOSLB PROTOCOL 

#define BNS_FILE_SIGN "LMXBOSLB"
#define BNS_FILE_VER 3

#pragma pack(push, 1)

struct LMXBOSLB_HEADER {
	uint8 sing[8];
	uint32 ver;
	uint32 length;
};


#pragma pack(pop)

// ---------------------------------------------------------------------------



namespace Bits {
	inline uint32 makeInt32(uint8 b3, uint8 b2, uint8 b1, uint8 b0) {
		return (((b3 & 0xff) << 24) | ((b2 & 0xff) << 16) | ((b1 & 0xff) <<  8) | ((b0 & 0xff) <<  0));
	}

	inline uint32 toInt32L(uint8* buf, int i) {
		return makeInt32(buf[i + 3], buf[i + 2], buf[i + 1], buf[i + 0]);
	}

	inline uint32 toInt32B(uint8* buf, int i) {
		return makeInt32(buf[i + 0], buf[i + 1], buf[i + 2], buf[i + 3]);
	}
}

// ----------------------------------------------------------------------------

int warnings = 0;
int errors = 0;

const byte* encryptionKey = (const byte*)"bns_fgt_cb_2010!";

// trim from start
static inline std::wstring &ltrim(std::wstring &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<wint_t, int>(std::iswspace))));
        return s;
}

// trim from end
static inline std::wstring &rtrim(std::wstring &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<wint_t, int>(std::iswspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::wstring &trim(std::wstring &s) {
        return ltrim(rtrim(s));
}

void statusMessages(STATUS_CODES status, std::wstring *message = NULL)
{
	bool unrecoverable = false;
	switch (status)
	{
		case SUCCESS_GENERAL:
			break;
		case SUCCESS_EXIT:
			unrecoverable = true;
			break;
		case SUCCESS_DECRYPTED:
			std::wcout << L"SUCCESS: File decrypted successfully!" << std::endl;
			break;
		case SUCCESS_ENCRYPTED:
			std::wcout << L"SUCCESS: File encrypted successfully!" << std::endl;
			break;
		case INFO_DECRYPTION_STARTED:
			std::wcout << L"INFO: Starting decryption process..." << std::endl;
			break;
		case INFO_ENCRYPTION_STARTED:
			std::wcout << L"INFO: Starting encryption process..." << std::endl;
			break;
		case WARNING_ASSUMING_DECRYPTION:
			warnings++;
			std::wcout << L"WARNING: Unknown mode specified, assuming decryption..." << std::endl;
			break;
		case WARNING_DECRYPTION_FILE_SIGNATURE_MISMATCH:
			warnings++;
			std::wcout << L"WARNING: File";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" signature mismatch! Trying to proceed..." << std::endl;
			break;
		case WARNING_DECRYPTION_FILE_VERSION_MISMATCH:
			warnings++;
			std::wcout << L"WARNING: File";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" version mismatch! Trying to proceed..." << std::endl;
			break;
		case WARNING_DECRYPTION_HEADER_CHECK_FAILED:
			warnings++;
			std::wcout << L"WARNING: Checking header size of file";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" was unsuccessful! Trying to proceed..." << std::endl;
			break;
		case ERROR_DECRYPTION_UNCOMPRESS_FAILED:
			errors++;
			std::wcout << L"ERROR: Couldn't decompress decrypted file";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L"! Decryption error? Trying to decrypt other files..." << std::endl;
			break;
		case ERROR_DECRYPTION_FILE_SIZE_MISMATCH:
			errors++;
			std::wcout << L"ERROR: File";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" filesize mismatch! Aborting deserialization..." << std::endl;
			break;
		case ERROR_ENCRYPTION_XML_ERROR:
			errors++;
			std::wcout << L"ERROR: File";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" XML parsing failed! Aborting serialization..." << std::endl;
			break;
		case ERROR_FILE_CREATION_FAILED:
			errors++;
			std::wcout << L"ERROR: Couldn't create destination file";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L"! Trying to decrypt other files..." << std::endl;
			break;
		case ERROR_UNRECOVERABLE_NOT_ENOUGH_ARGUMENTS:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: Not enough arguments!" << std::endl;
			std::wcout << L"USAGE: dated.exe path_to_dat_file -- path_to_decrypted_folder -- mode" << std::endl;
			std::wcout << L"path_to_dat_file - the path to the encrypted DAT BnS file. If decrypting, this file will be used as source. If encrypting, this file will be used as destination." << std::endl;
			std::wcout << L"path_to_decrypted_folder - the path to the folder, which contains decrypted data from the DAT BnS file. If decrypting, this folder will be used as destination. If encrypting, this folder will be used as source." << std::endl;
			std::wcout << L"mode - the mode of the processing. d - decrypting, e - encrypting, unknown values considered as decrypting." << std::endl;
			std::wcout << L"Example:" << std::endl;
			std::wcout << L"dated.exe C:\\BnS\\content\\data\\config.dat -- C:\\unencrypted -- d" << std::endl;
			std::wcout << L"This will decrypt the config.dat and put all its contents to C:\\unencrypted" << std::endl;
			std::wcout << L"dated.exe C:\\xml.dat -- C:\\xmldata -- d" << std::endl;
			std::wcout << L"This will encrypt all files from the C:\\xmldata and put them into C:\\xml.dat" << std::endl;
			break;
		case ERROR_UNRECOVERABLE_FILE_CREATION_FAILED:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: Couldn't create destination file";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L"!" << std::endl;
			break;
		case ERROR_UNRECOVERABLE_FILE_ACCESS_FAILED:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: File";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" access error!" << std::endl;
			break;
		case ERROR_UNRECOVERABLE_FOLDER_ACCESS_ERROR:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: Couldn't access folder";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L" with decrypted data!" << std::endl;
			break;
		case ERROR_UNRECOVERABLE_NOT_SUPPORTED_YET:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: That feature is not supported yet!" << std::endl;
			break;
		case ERROR_UNRECOVERABLE_UNKNOWN:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: Unknown error occured!" << std::endl;
			break;
		case ERROR_UNRECOVERABLE_DECRYPTION_UNCOMPRESS_FAILED:
			errors++;
			unrecoverable = true;
			std::wcout << L"FATAL ERROR: Couldn't decompress decrypted file";
			if (message != NULL)
			{
				std::wcout << L" \"" << *message << L"\"";
			}
			std::wcout << L"! Decryption error?" << std::endl;
			break;
		default:
			errors++;
			std::wcout << L"ERROR: Unknown error occured!" << std::endl;
			break;
	}
	if (unrecoverable)
	{
		std::wcout << L"INFO: Exiting. Errors: " << errors << ", warnings: " << warnings << std::endl;
		exit(0);
	}
}

void CreateSubDirectory(std::wstring fullpath, std::wstring currentpath)
{
	if (currentpath.find(L"\\") != currentpath.npos)
	{
		int result = CreateDirectory(currentpath.c_str(), NULL);
		if (result != 0 || GetLastError() == ERROR_ALREADY_EXISTS)
		{
			unsigned int position = fullpath.find(L"\\", currentpath.size() + 1);
			if (position == fullpath.npos)
			{
				return;
			}
			position++;
			if (position < fullpath.size())
			{
				currentpath.assign(fullpath.substr(0, position));
				CreateSubDirectory(fullpath, currentpath);
			}
		}
	}
}

void CreateSubDirectories(std::wstring path, std::wstring parentpath)
{
	unsigned int position = path.find(L"\\", parentpath.size() + 1);
	if (position == path.npos)
	{
		return;
	}
	position++;
	if (position < path.size())
	{
		std::wstring currentpath = path.substr(0, position);
		CreateSubDirectory(path, currentpath);
	}
}

STATUS_CODES checkArguments(int argc, wchar_t* argv[], std::wstring &arg_inputFile, std::wstring &arg_outputFolder, char &mode)
{
	if (argc <= 5)
	{
		statusMessages(ERROR_UNRECOVERABLE_NOT_ENOUGH_ARGUMENTS);
	}
	int state = 0;
	mode = 'u';
	for (int i = 1; i < argc; i++)
	{
		std::wstring temporary(argv[i]);
		if (temporary.compare(L"--") == 0)
		{
			state++;
		}
		else if (state == 0)
		{
			arg_inputFile.append(temporary);
			arg_inputFile.append(L" ");
		}
		else if (state == 1)
		{
			arg_outputFolder.append(temporary);
			arg_outputFolder.append(L" ");
		}
		else
		{
			if (temporary.compare(L"d") == 0)
			{
				mode = 'd';
			}
			if (temporary.compare(L"e") == 0)
			{
				mode = 'e';
			}
			break;
		}
	}

	rtrim(arg_inputFile);
	rtrim(arg_outputFolder);

	if(state < 2)
	{
		statusMessages(ERROR_UNRECOVERABLE_NOT_ENOUGH_ARGUMENTS);
	}

	if (CreateDirectory(arg_outputFolder.c_str(), NULL) == 0 && GetLastError() != ERROR_ALREADY_EXISTS)
	{
		statusMessages(ERROR_UNRECOVERABLE_FOLDER_ACCESS_ERROR, &arg_outputFolder);
	}
	
	if (mode == 'u')
	{
		statusMessages(WARNING_ASSUMING_DECRYPTION);
		mode = 'd';
	}

	return SUCCESS_GENERAL;
}

void checkFileErrors(std::ios *file)
{
	bool fail = file->fail();
	bool eof = file->eof();
	if (file->fail() || file->eof())
	{
		statusMessages(ERROR_UNRECOVERABLE_FILE_ACCESS_FAILED);
	}
}

void readFromFile(std::ifstream &file, std::streampos &position, std::streamsize size, byte* destination)
{
	file.seekg(position);
	file.read((char*)destination, size);
	checkFileErrors(&file);
	file.seekg(position+size);
	position = file.tellg();
}

void performXoring(byte* data, unsigned int size)
{
	byte xorArray[16] = {0xA4, 0x9F, 0xD8, 0xB3, 0xF6, 0x8E, 0x39, 0xC2, 0x2D, 0xE0, 0x61, 0x75, 0x5C, 0x4B, 0x1A, 0x07};
	unsigned int length = size*2;
	int lengthLeft = length;
	while (lengthLeft > 0)
	{
		for (unsigned int i = 0; i < lengthLeft && i < 16; i++)
		{
			data[length - lengthLeft + i] = data[length - lengthLeft + i] ^ xorArray[i];
		}
		lengthLeft = lengthLeft - 16;
	}
}

void headerErrorDump(byte* inputData, unsigned int inputLength, std::wstring filename, std::wstring error) {
	std::ofstream outputFile(filename + L".error." + error, std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
	outputFile.write((char*)inputData, inputLength);
	outputFile.close();	
}

STATUS_CODES configUnserializeXml(byte* inputData, unsigned int inputLength, byte* &outputData, unsigned int &outputLength, std::wstring filename)
{
	int ver = Bits::toInt32L(inputData, 8);
	int length = Bits::toInt32L(inputData, 12);

	if (strncmp((const char*)&inputData[0], BNS_FILE_SIGN, 8)!=0)
	{
		#ifdef DUMP_HEADER_ERROR
			headerErrorDump(inputData, inputLength, filename, L"sign");		
		#endif

		statusMessages(WARNING_DECRYPTION_FILE_SIGNATURE_MISMATCH, &filename);
	}	
	if (ver != BNS_FILE_VER)
	{
		#ifdef DUMP_HEADER_ERROR
			headerErrorDump(inputData, inputLength, filename, L"ver");		
		#endif

		statusMessages(WARNING_DECRYPTION_FILE_VERSION_MISMATCH, &filename);
	}
	if (length != inputLength)
	{
		statusMessages(ERROR_DECRYPTION_FILE_SIZE_MISMATCH, &filename);
		return ERROR_DECRYPTION_FILE_SIZE_MISMATCH;
	}
	byte* inputDataSerializedData = inputData + sizeof(int) + 2 * *((unsigned int*)(inputData + 0x51)) + 0x51;
	unsigned int mode = 1;
	std::wstring parsedXml(L"<?xml version='1.0' encoding='utf-8'?>\r\n");
	std::stack<std::pair<unsigned int, std::wstring>> openedTags;
	while (inputDataSerializedData < inputData + inputLength)
	{
		if (mode == 1)
		{
			unsigned int paramNumber = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(unsigned int);
			std::map<std::wstring, std::wstring> parameters;
			for (unsigned int i = 0; paramNumber > i; i++)
			{
				unsigned int nameLength = *((unsigned int*)inputDataSerializedData);
				inputDataSerializedData += sizeof(nameLength);
				performXoring(inputDataSerializedData, nameLength);
				std::wstring paramName((wchar_t*)inputDataSerializedData, nameLength);
				inputDataSerializedData += sizeof(wchar_t) * nameLength;
				unsigned int valueLength = *((unsigned int*)inputDataSerializedData);
				inputDataSerializedData += sizeof(nameLength);
				performXoring(inputDataSerializedData, valueLength);
				std::wstring paramValue((wchar_t*)inputDataSerializedData, valueLength);
				inputDataSerializedData += sizeof(wchar_t) * valueLength;
				parameters.insert(std::pair<std::wstring, std::wstring>(paramName, paramValue));
			}
			byte checkByte = inputDataSerializedData[0];
			if (checkByte != 1)
			{
				std::wcout << L"WARNING: Unexpected value while parsing! checkByte, file: " << filename << L", offset: " << std::hex << inputDataSerializedData - inputData << std::endl;
			}
			inputDataSerializedData += sizeof(checkByte);
			unsigned int tagNameSize = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(tagNameSize);
			performXoring(inputDataSerializedData, tagNameSize);
			std::wstring tagName((wchar_t*)inputDataSerializedData, tagNameSize);
			inputDataSerializedData += sizeof(wchar_t) * tagNameSize;
			unsigned int numberOfChilds = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(numberOfChilds);
			unsigned int tagId = *((unsigned int*)inputDataSerializedData);
			//std::wcout << L"INFO: tag " << tagName << L" is using tag code " << std::hex << tagCode << " with tag id " << std::hex << tagId << L" on offset " << std::hex << inputDataSerializedData - inputData << L" file " << filename << std::endl;
			inputDataSerializedData += sizeof(tagId);
			mode = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(mode);

			while (true)
			{
				if (openedTags.size() > 0)
				{
					std::pair<unsigned int, std::wstring> levelData = openedTags.top();
					openedTags.pop();
					if (levelData.first == 0)
					{
						for (unsigned int i = 0; openedTags.size() > i; i++)
						{
							parsedXml.append(L"\t");
						}
						parsedXml.append(L"</");
						parsedXml.append(levelData.second);
						parsedXml.append(L">\r\n");
					}
					else
					{
						levelData.first--;
						openedTags.push(levelData);
						break;
					}
				}
				else
				{
					break;
				}
			}

			for (unsigned int i = 0; openedTags.size() > i; i++)
			{
				parsedXml.append(L"\t");
			}
			parsedXml.append(L"<");
			parsedXml.append(tagName);
			std::map<std::wstring, std::wstring>::const_iterator itr;

			for (itr = parameters.begin(); itr != parameters.end(); ++itr)
			{
				parsedXml.append(L" ");
				parsedXml.append(itr->first);
				parsedXml.append(L"=\"");
				parsedXml.append(itr->second);
				parsedXml.append(L"\"");
			}
			if (numberOfChilds == 0)
			{
				parsedXml.append(L"/");
			}
			parsedXml.append(L">\r\n");

			if (numberOfChilds > 0)
			{
				std::pair<unsigned int, std::wstring> levelData(numberOfChilds, tagName);
				openedTags.push(levelData);
			}
		}
		else if (mode == 2)
		{
			unsigned int prependLength = *((unsigned int*)inputDataSerializedData);
			performXoring(&inputDataSerializedData[4], prependLength);
			std::wstring prependData((wchar_t*)&inputDataSerializedData[4], prependLength);
			trim(prependData);
			inputDataSerializedData += sizeof(prependLength) + prependLength * sizeof(wchar_t);
			byte checkByte = inputDataSerializedData[0];
			if (checkByte != 1)
			{
				std::wcout << L"WARNING: Unexpected value while parsing! checkByte, file: " << filename << L", offset: " << std::hex << inputDataSerializedData - inputData << std::endl;
			}
			inputDataSerializedData += sizeof(checkByte);
			unsigned int formatLength = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(formatLength);
			performXoring(inputDataSerializedData, formatLength);
			std::wstring format((wchar_t*)inputDataSerializedData, formatLength);
			if (format.compare(L"text") != 0)
			{
				std::wcout << L"WARNING: Unexpected value while parsing! format, file: " << filename << L", offset: " << std::hex << inputDataSerializedData - inputData << std::endl;
			}
			inputDataSerializedData += sizeof(wchar_t) * formatLength;
			unsigned int numberOfChilds = *((unsigned int*)inputDataSerializedData);
			if (numberOfChilds != 0)
			{
				std::wcout << L"WARNING: Unexpected value while parsing! numberOfChilds != 0 for text value, file: " << filename << L", offset: " << std::hex << inputDataSerializedData - inputData << std::endl;
			}
			inputDataSerializedData += sizeof(numberOfChilds);
			unsigned int nextTagId = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(nextTagId);
			if (inputDataSerializedData < inputData + inputLength)
				mode = *((unsigned int*)inputDataSerializedData);
			inputDataSerializedData += sizeof(mode);
			
			
			while (true)
			{
				if (openedTags.size() > 0)
				{
					std::pair<unsigned int, std::wstring> levelData = openedTags.top();
					openedTags.pop();
					if (levelData.first == 0)
					{
						for (unsigned int i = 0; openedTags.size() > i; i++)
						{
							parsedXml.append(L"\t");
						}
						parsedXml.append(L"</");
						parsedXml.append(levelData.second);
						parsedXml.append(L">\r\n");
					}
					else
					{
						levelData.first--;
						openedTags.push(levelData);
						break;
					}
				}
				else
				{
					break;
				}
			}
			
			
			if (prependData.size() > 0)
			{
				for (unsigned int i = 0; openedTags.size() > i; i++)
				{
					parsedXml.append(L"\t");
				}
				parsedXml.append(prependData);
				parsedXml.append(L"\r\n");
			}
		}
		else
		{
			std::wcout << L"WARNING: Unexpected value while parsing! mode, file: " << filename << L", offset: " << std::hex << inputDataSerializedData - inputData << std::endl;
		}
	}

	while (openedTags.size() != 0)
	{
		for (unsigned int i = 1; openedTags.size() > i; i++)
		{
			parsedXml.append(L"\t");
		}
		parsedXml.append(L"</");
		std::pair<unsigned int, std::wstring> levelData = openedTags.top();
		parsedXml.append(levelData.second);
		openedTags.pop();
		parsedXml.append(L">\r\n");
	}

	outputLength = parsedXml.size();
	unsigned int sizeRequired = WideCharToMultiByte(CP_UTF8, 0, parsedXml.c_str(), parsedXml.length(), NULL, 0,  NULL, NULL);
	outputData = new byte[sizeRequired+1];
	outputData[sizeRequired] = 0;
	outputLength = WideCharToMultiByte(CP_UTF8, 0, parsedXml.c_str(), parsedXml.length(), (char*)outputData, sizeRequired,  NULL, NULL);
	return SUCCESS_GENERAL;
}

STATUS_CODES configDecryptFile(std::ifstream &file, std::streampos header, std::wstring folder, std::wstring filename, header_file_opts *opts)
{
	std::wcout << L"INFO: File " << filename << L" detected, trying to decrypt..." << std::endl;
	byte* encryptedFiledata = new byte[opts->compressed];
    memset(encryptedFiledata, 0, opts->compressed);
	byte* decryptedFiledata = new byte[opts->compressed];
    memset(decryptedFiledata, 0, opts->compressed);
	std::streampos offset(opts->offset);
	std::streampos position = header + offset;
	readFromFile(file, position, opts->compressed, encryptedFiledata);

	CryptoPP::ECB_Mode<CryptoPP::AES>::Decryption decryptor;

	decryptor.SetKey(encryptionKey, strlen((const char*)encryptionKey));
	decryptor.ProcessData(decryptedFiledata, encryptedFiledata, opts->compressed);

	byte* uncompressedFiledata = new byte[opts->uncompressed];

	z_stream_s zlibStream;
	zlibStream.avail_in = opts->compressedNoPadding;
	zlibStream.next_in = decryptedFiledata;
	zlibStream.avail_out = opts->uncompressed;
	zlibStream.next_out = uncompressedFiledata;
	zlibStream.zalloc = NULL;
	zlibStream.zfree = NULL;
	inflateInit(&zlibStream);
	int result = inflate(&zlibStream, Z_FINISH);
	if (result != Z_STREAM_END)
	{
		statusMessages(ERROR_DECRYPTION_UNCOMPRESS_FAILED, &filename);
	}
	inflateEnd(&zlibStream);

	unsigned int filesize = opts->uncompressed;

	if (filename.find(L".xml") != filename.npos || filename.find(L".x16") != filename.npos)
	{
		byte* parsedXml;
		unsigned int parsedLength;
		if(SUCCESS_GENERAL == configUnserializeXml(uncompressedFiledata, filesize, parsedXml, parsedLength, filename))
		{
			delete [] uncompressedFiledata;
			uncompressedFiledata = parsedXml;
			filesize = parsedLength;
		}
	}

	std::wstring outputFilename(folder);
	outputFilename.append(L"\\");
	outputFilename.append(filename);

	CreateSubDirectories(outputFilename, folder);

	std::ofstream outputFile(outputFilename, std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);
	if (!outputFile.is_open())
	{
		delete [] encryptedFiledata;
		delete [] decryptedFiledata;
		delete [] uncompressedFiledata;
		statusMessages(ERROR_FILE_CREATION_FAILED, &outputFilename);
		return ERROR_FILE_CREATION_FAILED;
	}
	outputFile.write((char*)uncompressedFiledata, filesize);
	outputFile.close();

	delete [] encryptedFiledata;
	delete [] decryptedFiledata;
	delete [] uncompressedFiledata;

	return SUCCESS_GENERAL;
}

STATUS_CODES configEncryptListAllFiles(std::wstring folderpath, std::wstring basefolder, std::vector<std::wstring>* filelist)
{
	WIN32_FIND_DATA findData;
	HANDLE findHandle = INVALID_HANDLE_VALUE;

	std::wstring findPath(folderpath);

	findPath.append(L"\\*");

	findHandle = FindFirstFile(findPath.c_str(), &findData);

	if (findHandle == INVALID_HANDLE_VALUE)
	{
		statusMessages(ERROR_UNRECOVERABLE_FOLDER_ACCESS_ERROR, &folderpath);
	}

	do
	{
		if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			std::wstring subfoldername(findData.cFileName);
			if (subfoldername.compare(L".") != 0 && subfoldername.compare(L"..") != 0)
			{
				std::wstring subfolderpath(folderpath);
				subfolderpath.append(L"\\");
				subfolderpath.append(subfoldername);
				std::wstring subbasefolder(basefolder);
				if (basefolder.size() > 0)
				{
					subbasefolder.append(L"\\");
				}
				subbasefolder.append(subfoldername);
				configEncryptListAllFiles(subfolderpath, subbasefolder, filelist);
			}
		}
		else
		{
			std::wstring filename(basefolder);
			if (basefolder.size() > 0)
			{
				filename.append(L"\\");
			}
			filename.append(findData.cFileName);
			filelist->push_back(filename);
		}
	}
	while (FindNextFile(findHandle, &findData) != 0);

	return SUCCESS_GENERAL;
}

STATUS_CODES configSerializeXmlLevel(pugi::xml_node &node, std::vector<tag_entry_serialxml>* tags)
{
	tag_entry_serialxml tag;
	tag.tagname = pugi::as_wide(node.name());
	tag.childs = std::distance(node.begin(), node.end());
	tag.mode = 1;
	tag.childMode = 1;
	for (pugi::xml_attribute_iterator ait = node.attributes_begin(); ait != node.attributes_end(); ++ait)
    {
		tag.tagparams.insert(std::pair<std::wstring, std::wstring>(pugi::as_wide(ait->name()), pugi::as_wide(ait->value())));
    }
	bool subtextNode = false;
	std::wstring valueData(pugi::as_wide(node.child_value()));
	if (tag.childs == 1 && valueData.size() > 0)
	{
		tag.childs = 1;
		tag.childMode = 2;
		subtextNode = true;
	}
	tags->push_back(tag);
	if (subtextNode)
	{
		tag.childs = 0;
		tag_entry_serialxml texttag;
		texttag.childs = 0;
		texttag.mode = 2;
		texttag.childMode = 1;
		texttag.tagname = valueData;
		tags->push_back(texttag);
	}
	if (tag.childs > 0)
	{
		for (pugi::xml_node_iterator nit = node.begin(); nit != node.end(); ++nit)
		{
			configSerializeXmlLevel(*nit, tags);
		}
	}
	return SUCCESS_GENERAL;
}

STATUS_CODES configSerializeXml(byte* input, unsigned int inputSize, byte* &output, unsigned int &outputSize, std::wstring filename)
{
	std::vector<tag_entry_serialxml> tags;
	pugi::xml_document document;
	if (!document.load_buffer_inplace(input, inputSize))
	{
		statusMessages(ERROR_ENCRYPTION_XML_ERROR, &filename);
		return ERROR_ENCRYPTION_XML_ERROR;
	}
	if (SUCCESS_GENERAL != configSerializeXmlLevel(document.document_element(), &tags))
	{
		statusMessages(ERROR_ENCRYPTION_XML_ERROR, &filename);
		return ERROR_ENCRYPTION_XML_ERROR;
	}
	std::vector<std::pair<unsigned int, byte*>> serializedTags;
	unsigned int fullsize = 0;
	fullsize += 0x51; // Length of header
	fullsize += 4; // INT length of Filename
	fullsize += sizeof(wchar_t) * filename.size(); // WCHAR_T ARRAY Filename
	for (unsigned int i = 0; i < tags.size(); i++)
	{
		if (tags[i].mode == 1)
		{
			unsigned int serializedSize = 0;
			serializedSize += 4; // INT number of parameters
			for (std::map<std::wstring, std::wstring>::iterator it = tags[i].tagparams.begin(); it != tags[i].tagparams.end(); ++it)
			{
				serializedSize += 4; // INT length of attribute key
				serializedSize += it->first.size() * sizeof(wchar_t); // WCHAR_T ARRAY attribute key
				serializedSize += 4; // INT length of attribute value
				serializedSize += it->second.size() * sizeof(wchar_t); // WCHAR_T ARRAY attribute value
			}
			serializedSize += 1; // BYTE checkByte
			serializedSize += 4; // INT length of tag name
			serializedSize += tags[i].tagname.size() * sizeof(wchar_t); // WCHAR_T ARRAY tag name
			serializedSize += 4; // INT number of childs
			serializedSize += 4; // INT tagId
			if ((i + 1) < tags.size())
			{
				serializedSize += 4; // INT next mode
			}

			byte* serializedTag = new byte[serializedSize];
			byte* serializedTagOrigAddr = serializedTag;
			*((unsigned int*)&serializedTag[0]) = tags[i].tagparams.size(); // INT number of parameters
			serializedTag += 4;
			for (std::map<std::wstring, std::wstring>::iterator it = tags[i].tagparams.begin(); it != tags[i].tagparams.end(); ++it)
			{
				*((unsigned int*)&serializedTag[0]) = it->first.size(); // INT length of attribute key
				serializedTag += 4;
				memcpy(&serializedTag[0], it->first.c_str(), it->first.size() * sizeof(wchar_t)); // WCHAR_T ARRAY attribute key
				performXoring(&serializedTag[0], it->first.size());
				serializedTag += it->first.size() * sizeof(wchar_t);
				*((unsigned int*)&serializedTag[0]) = it->second.size(); // INT length of attribute value
				serializedTag += 4;
				memcpy(&serializedTag[0], it->second.c_str(), it->second.size() * sizeof(wchar_t)); // WCHAR_T ARRAY attribute value
				performXoring(&serializedTag[0], it->second.size());
				serializedTag += it->second.size() * sizeof(wchar_t);
			}
			serializedTag[0] = 1; // BYTE checkByte
			serializedTag += 1;
			*((unsigned int*)&serializedTag[0]) = tags[i].tagname.size(); // INT length of tag name
			serializedTag += 4;
			memcpy(&serializedTag[0], tags[i].tagname.c_str(), tags[i].tagname.size() * sizeof(wchar_t)); // WCHAR_T ARRAY tag name
			performXoring(&serializedTag[0], tags[i].tagname.size());
			serializedTag += tags[i].tagname.size() * sizeof(wchar_t); // WCHAR_T ARRAY tag name
			*((unsigned int*)&serializedTag[0]) = tags[i].childs; // INT number of childs
			serializedTag += 4;
			*((unsigned int*)&serializedTag[0]) = i + 1; // INT tagId
			if ((i + 1) < tags.size())
			{
				serializedTag += 4;
				*((unsigned int*)&serializedTag[0]) = tags[i].childMode; // INT next mode
			}

			fullsize += serializedSize;
			serializedTags.push_back(std::pair<unsigned int, byte*>(serializedSize, serializedTagOrigAddr));
		}
		else if (tags[i].mode == 2)
		{
			unsigned int serializedSize = 0;
			serializedSize += 4; // INT length of text data
			serializedSize += tags[i].tagname.size() * sizeof(wchar_t); // WCHAR_T ARRAY text data
			serializedSize += 1; // BYTE checkByte
			serializedSize += 4; // INT length of tag name
			serializedSize += 4 * sizeof(wchar_t); // WCHAR_T ARRAY tag name "text"
			serializedSize += 4; // INT number of childs
			serializedSize += 4; // INT tagId
			if ((i + 1) < tags.size())
			{
				serializedSize += 4; // INT next mode
			}

			byte* serializedTag = new byte[serializedSize];
			byte* serializedTagOrigAddr = serializedTag;
			*((unsigned int*)&serializedTag[0]) = tags[i].tagname.size(); // INT length of text data
			serializedTag += 4;
			memcpy(&serializedTag[0], tags[i].tagname.c_str(), tags[i].tagname.size() * sizeof(wchar_t)); // WCHAR_T ARRAY text data
			performXoring(&serializedTag[0], tags[i].tagname.size());
			serializedTag += tags[i].tagname.size() * sizeof(wchar_t);
			serializedTag[0] = 1; // BYTE checkByte
			serializedTag += 1;
			*((unsigned int*)&serializedTag[0]) = 4; // INT length of tag name "text" (4)
			serializedTag += 4;
			memcpy(&serializedTag[0], L"text", 4 * sizeof(wchar_t)); // WCHAR_T ARRAY tag name "text"
			performXoring(&serializedTag[0], 4);
			serializedTag += 4 * sizeof(wchar_t); // WCHAR_T ARRAY tag name
			*((unsigned int*)&serializedTag[0]) = tags[i].childs; // INT number of childs
			serializedTag += 4;
			*((unsigned int*)&serializedTag[0]) = i + 1; // INT tagId
			if ((i + 1) < tags.size())
			{
				serializedTag += 4;
				*((unsigned int*)&serializedTag[0]) = tags[i].childMode; // INT next mode
			}

			fullsize += serializedSize;
			serializedTags.push_back(std::pair<unsigned int, byte*>(serializedSize, serializedTagOrigAddr));
		}
	}
	tags.clear();
	byte* fulldata = new byte[fullsize];
	byte* fulldataOrigAddr = fulldata;
	memset(fulldata, 0, fullsize);
	strncpy((char*)&fulldata[0], BNS_FILE_SIGN, 8);
	*((unsigned int*)&fulldata[8]) = BNS_FILE_VER;
	*((unsigned int*)&fulldata[12]) = fullsize;
	fulldata[0x50] = 1;
	*((unsigned int*)&fulldata[0x51]) = filename.size();
	fulldata += 0x51 + sizeof(unsigned int);
	memcpy(fulldata, filename.c_str(), filename.size() * sizeof(wchar_t));
	fulldata += filename.size() * sizeof(wchar_t);
	for (unsigned int i = 0; i < serializedTags.size(); i++)
	{
		memcpy(fulldata, serializedTags[i].second, serializedTags[i].first);
		delete [] serializedTags[i].second;
		fulldata += serializedTags[i].first;
	}
	serializedTags.clear();
	output = fulldataOrigAddr;
	outputSize = fullsize;
	return SUCCESS_GENERAL;
}

STATUS_CODES configEncrypt(std::wstring filename, std::wstring folderpath)
{
    std::vector<std::wstring> filelist;
	configEncryptListAllFiles(folderpath, L"", &filelist);
	std::vector<file_entry> filedata;
	byte* fileStore = NULL;
	unsigned int headerSize = 0;
	for (unsigned int i = 0; i < filelist.size(); i++)
	{
		std::wcout << L"INFO: File " << filelist[i] << L" detected, trying to encrypt..." << std::endl;
        std::wstring subfile(folderpath);
		subfile.append(L"\\");
		subfile.append(filelist[i]);
		std::ifstream filesource(subfile, std::ifstream::in | std::ifstream::binary);
		if (!filesource.is_open())
		{
			statusMessages(ERROR_UNRECOVERABLE_FILE_ACCESS_FAILED);
		}
		filesource.seekg(0, std::ifstream::end);
		unsigned int filesourceSize = filesource.tellg();
		filesource.seekg(0, std::ifstream::beg);
		byte* filesourceData = new byte[filesourceSize];
		filesource.read((char*)filesourceData, filesourceSize);
		byte* uncompressed = filesourceData;
		unsigned int uncompressedSize = filesourceSize;
		if (subfile.find(L".xml") != subfile.npos || subfile.find(L".x16") != subfile.npos)
		{
			byte* serialized = NULL;
			unsigned int serializedSize = 0;
			if (SUCCESS_GENERAL == configSerializeXml(filesourceData, filesourceSize, serialized, serializedSize, filelist[i]))
			{
				delete[] filesourceData;
				uncompressed = serialized;
				uncompressedSize = serializedSize;
			}
		}
		z_stream_s zlibStream;
		zlibStream.zalloc = NULL;
		zlibStream.zfree = NULL;
		deflateInit(&zlibStream, Z_DEFAULT_COMPRESSION);
		zlibStream.avail_in = uncompressedSize;
		zlibStream.next_in = uncompressed;
		byte* compressed = new byte[uncompressedSize];
		zlibStream.avail_out = uncompressedSize;
		zlibStream.next_out = compressed;
		int result = deflate(&zlibStream, Z_FINISH);
		unsigned int compressedSize = zlibStream.next_out - compressed;
		deflateEnd(&zlibStream);
		int paddedSize = compressedSize;
		if (paddedSize % 16 != 0)
		{
			paddedSize += 16 - (paddedSize % 16);
		}

		delete [] uncompressed;

		byte* crypted = new byte[paddedSize];
		CryptoPP::ECB_Mode<CryptoPP::AES>::Encryption encryptor;

		encryptor.SetKey(encryptionKey, strlen((const char*)encryptionKey));
		encryptor.ProcessData(crypted, compressed, paddedSize);

		delete [] compressed;

		file_entry entry;
		entry.filedata = crypted;
		entry.filename = filelist[i];
		memset(&entry.fileopts, 0, sizeof(entry.fileopts));
		entry.fileopts.somenum = 2;
		entry.fileopts.somebool = true;
		entry.fileopts.somebool2 = true;
		entry.fileopts.somebool3 = false;
		entry.fileopts.uncompressed = uncompressedSize;
		entry.fileopts.compressedNoPadding = compressedSize;
		entry.fileopts.compressed = paddedSize;
		filedata.push_back(entry);
		headerSize += sizeof(entry.fileopts) + sizeof(unsigned int) + sizeof(wchar_t) * entry.filename.size();
	}
	byte* headerUncompressed = new byte[headerSize];
	byte* headerUncompressedPointer = headerUncompressed;
	unsigned int currentOffset = 0;
	for (unsigned int i = 0; i < filedata.size(); i++)
	{
		*((unsigned int*)headerUncompressedPointer) = filedata[i].filename.size();
		headerUncompressedPointer += sizeof(unsigned int);
		memcpy(headerUncompressedPointer, filedata[i].filename.c_str(), filedata[i].filename.size() * sizeof(wchar_t));
		headerUncompressedPointer += filedata[i].filename.size() * sizeof(wchar_t);
		filedata[i].fileopts.offset = currentOffset;
		currentOffset += filedata[i].fileopts.compressed;
		header_file_opts entryfileopts = filedata[i].fileopts;
		memcpy(headerUncompressedPointer, &entryfileopts, sizeof(entryfileopts));
		headerUncompressedPointer += sizeof(entryfileopts);
	}
	z_stream_s zlibStream;
	zlibStream.zalloc = NULL;
	zlibStream.zfree = NULL;
	deflateInit(&zlibStream, Z_DEFAULT_COMPRESSION);
	zlibStream.avail_in = headerSize;
	zlibStream.next_in = headerUncompressed;
	byte* compressed = new byte[headerSize];
	zlibStream.avail_out = headerSize;
	zlibStream.next_out = compressed;
	int result = deflate(&zlibStream, Z_FINISH);
	unsigned int compressedSize = zlibStream.next_out - compressed;
	deflateEnd(&zlibStream);
	int paddedSize = compressedSize;
	if (paddedSize % 16 != 0)
	{
		paddedSize += 16 - (paddedSize % 16);
	}

	delete [] headerUncompressed;

	byte* crypted = new byte[paddedSize];
    memset(crypted, 0, paddedSize);
	CryptoPP::ECB_Mode<CryptoPP::AES>::Encryption encryptor;

	encryptor.SetKey(encryptionKey, strlen((const char*)encryptionKey));
	encryptor.ProcessData(crypted, compressed, paddedSize);

	delete [] compressed;

	std::ofstream cryptedfile(filename, std::ofstream::trunc | std::ofstream::binary | std::ofstream::out);
    unsigned int position = cryptedfile.tellp();
	if(!cryptedfile.is_open())
	{
		statusMessages(ERROR_UNRECOVERABLE_FILE_CREATION_FAILED, &filename);
	}
	byte baseHeader[0x59];
	memset(baseHeader, 0, 0x59);
    
    strncpy((char*)&baseHeader[0], "UOSEDALB", 8);
    *((int*)&baseHeader[8]) = 2;

	*((unsigned int*)&baseHeader[0x11]) = currentOffset;
	*((unsigned int*)&baseHeader[0x15]) = filedata.size();
	baseHeader[0x19] = 1;
	baseHeader[0x1A] = 1;
    
    cryptedfile.write((char*)baseHeader, 0x59);
    position = cryptedfile.tellp();
    
    cryptedfile.write((char*)&paddedSize, 4);
    position = cryptedfile.tellp();
    
    cryptedfile.write((char*)&headerSize, 4);
    position = cryptedfile.tellp();
    
    cryptedfile.write((char*)crypted, paddedSize);
    position = cryptedfile.tellp();
    
    delete [] crypted;
    
    unsigned int headerChecksum = position + 4;
    
    cryptedfile.write((char*)&headerChecksum, 4);
    position = cryptedfile.tellp();
    
    if (position != headerChecksum)
    {
        statusMessages(ERROR_UNRECOVERABLE_FILE_ACCESS_FAILED);
    }
    
    for (unsigned int i = 0; i < filedata.size(); i++)
	{
		cryptedfile.write((char*)filedata[i].filedata, filedata[i].fileopts.compressed);
        position = cryptedfile.tellp();
    }
    
    cryptedfile.close();

	return SUCCESS_GENERAL;
}

STATUS_CODES configDecrypt(std::wstring filename, std::wstring folderpath)
{
	std::ifstream encryptedfile(filename, std::ifstream::in | std::ifstream::binary);
	if (!encryptedfile.is_open())
	{
		statusMessages(ERROR_UNRECOVERABLE_DECRYPTION_FILE_OPEN_FAILED, &filename);
	}
	std::streampos position;
	encryptedfile.seekg(0, std::ifstream::beg);
	position = encryptedfile.tellg();
	byte baseHeader[0x59];
	readFromFile(encryptedfile, position, 0x59, baseHeader);
	unsigned int numberOfFiles = *((unsigned int*)&baseHeader[0x15]);
	if (strncmp((const char*)&baseHeader[0], "UOSEDALB", 8)!=0)
	{
		statusMessages(WARNING_DECRYPTION_FILE_SIGNATURE_MISMATCH, &filename);
	}
	if (*((int*)&baseHeader[8]) != 2)
	{
		statusMessages(WARNING_DECRYPTION_FILE_VERSION_MISMATCH, &filename);
	}
	unsigned int compressedMainHeaderSize;
	readFromFile(encryptedfile, position, 0x4, (byte*)&compressedMainHeaderSize);

	unsigned int uncompressedMainHeaderSize;
	readFromFile(encryptedfile, position, 0x4, (byte*)&uncompressedMainHeaderSize);
    
    unsigned int compressedMainHeaderSizePadded = compressedMainHeaderSize;
    
    if (compressedMainHeaderSizePadded % 16 != 0)
    {
        compressedMainHeaderSizePadded += 16 - (compressedMainHeaderSizePadded % 16);
    }

	byte* encryptedMainHeader = new byte[compressedMainHeaderSizePadded];
    memset(encryptedMainHeader, 0, compressedMainHeaderSizePadded);
	byte* decryptedMainHeader = new byte[compressedMainHeaderSizePadded];
    memset(decryptedMainHeader, 0, compressedMainHeaderSizePadded);

	readFromFile(encryptedfile, position, compressedMainHeaderSizePadded, (byte*)encryptedMainHeader);

	CryptoPP::ECB_Mode<CryptoPP::AES>::Decryption decryptor;

	decryptor.SetKey(encryptionKey, strlen((const char*)encryptionKey));
	decryptor.ProcessData(decryptedMainHeader, encryptedMainHeader, compressedMainHeaderSizePadded);

	byte* uncompressedMainHeader = new byte[uncompressedMainHeaderSize];

	z_stream_s zlibStream;
	zlibStream.avail_in = compressedMainHeaderSize;
	zlibStream.next_in = decryptedMainHeader;
	zlibStream.avail_out = uncompressedMainHeaderSize;
	zlibStream.next_out = uncompressedMainHeader;
	zlibStream.zalloc = NULL;
	zlibStream.zfree = NULL;
	inflateInit(&zlibStream);
	int result = inflate(&zlibStream, Z_FINISH);
	if(result != Z_STREAM_END)
	{
		statusMessages(ERROR_UNRECOVERABLE_DECRYPTION_UNCOMPRESS_FAILED, &filename);
	}
	inflateEnd(&zlibStream);
	unsigned int headerSizeCheck;
	readFromFile(encryptedfile, position, 0x4, (byte*)&headerSizeCheck);
	
	if(headerSizeCheck!=position)
	{
		statusMessages(WARNING_DECRYPTION_HEADER_CHECK_FAILED, &filename);
	}

	byte* headerParsePointer = uncompressedMainHeader;
	unsigned int parsedNumberOfFiles = 0;
	while (headerParsePointer < uncompressedMainHeader + uncompressedMainHeaderSize)
	{
		unsigned int *iFilenameLength = (unsigned int*)&headerParsePointer[0];
		std::wstring iFilename((wchar_t*)&headerParsePointer[0+sizeof(*iFilenameLength)], *iFilenameLength);
		header_file_opts *opts = (header_file_opts*)&headerParsePointer[0+sizeof(*iFilenameLength)+sizeof(wchar_t)*(*iFilenameLength)];
		configDecryptFile(encryptedfile, position, folderpath, iFilename, opts);
		parsedNumberOfFiles++;
		if (parsedNumberOfFiles >= numberOfFiles)
		{
			break;
		}
		headerParsePointer+=0+sizeof(*iFilenameLength)+sizeof(wchar_t)*(*iFilenameLength)+sizeof(*opts);
	}

	return SUCCESS_GENERAL;
}

int wmain(int argc, wchar_t* argv[])
{
	STATUS_CODES status;

	std::wstring arg_inputFile(L"");
	std::wstring arg_outputFolder(L"");
	char mode;

	status = checkArguments(argc, argv, arg_inputFile, arg_outputFolder, mode);

	if (status != SUCCESS_GENERAL)
	{
		statusMessages(ERROR_UNRECOVERABLE_UNKNOWN);
	}

	std::wcout << L"INFO: Using encrypted file: " << arg_inputFile << std::endl;
	std::wcout << L"INFO: Using decrypted folder: " << arg_outputFolder << std::endl;

	if (mode == 'e')
	{
		statusMessages(INFO_ENCRYPTION_STARTED);
		status = configEncrypt(arg_inputFile, arg_outputFolder);
		if (status != SUCCESS_GENERAL)
		{
			statusMessages(ERROR_UNRECOVERABLE_UNKNOWN);
		}
	}
	else
	{
		statusMessages(INFO_DECRYPTION_STARTED);
		status = configDecrypt(arg_inputFile, arg_outputFolder);
		if (status != SUCCESS_GENERAL)
		{
			statusMessages(ERROR_UNRECOVERABLE_UNKNOWN);
		}
	}
	
	statusMessages(SUCCESS_EXIT);
	return 0;
}

