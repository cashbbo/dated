// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <Windows.h>

// std
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <cwctype>
#include <locale>
#include <map>
#include <stack>
#include <sstream>
#include <vector>

// CryptoPP
#include "cryptlib.h"
#include "aes.h"
#include "modes.h"

// ZLib
#include "zlib.h"

// pugixml
#include "pugixml.hpp"

enum STATUS_CODES
{
	SUCCESS_GENERAL,
	SUCCESS_EXIT,
	SUCCESS_DECRYPTED,
	SUCCESS_ENCRYPTED,
	INFO_DECRYPTION_STARTED,
	INFO_ENCRYPTION_STARTED,
	WARNING_ASSUMING_DECRYPTION,
	WARNING_DECRYPTION_FILE_SIGNATURE_MISMATCH,
	WARNING_DECRYPTION_FILE_VERSION_MISMATCH,
	WARNING_DECRYPTION_HEADER_CHECK_FAILED,
	ERROR_FILE_CREATION_FAILED,
	ERROR_DECRYPTION_UNCOMPRESS_FAILED,
	ERROR_DECRYPTION_FILE_SIZE_MISMATCH,
	ERROR_ENCRYPTION_XML_ERROR,
	ERROR_UNRECOVERABLE_NOT_ENOUGH_ARGUMENTS,
	ERROR_UNRECOVERABLE_FOLDER_ACCESS_ERROR,
	ERROR_UNRECOVERABLE_FILE_CREATION_FAILED,
	ERROR_UNRECOVERABLE_FILE_ACCESS_FAILED,
	ERROR_UNRECOVERABLE_NOT_SUPPORTED_YET,
	ERROR_UNRECOVERABLE_DECRYPTION_FILE_OPEN_FAILED,
	ERROR_UNRECOVERABLE_DECRYPTION_UNCOMPRESS_FAILED,
	ERROR_UNRECOVERABLE_UNKNOWN
};

struct header_file_opts
{
	byte somenum;
	bool somebool;
	bool somebool2;
	bool somebool3;
	unsigned int uncompressed;
	unsigned int compressedNoPadding;
	unsigned int compressed;
	unsigned int offset;
	byte zero[59];
};

struct file_entry
{
	std::wstring filename;
	header_file_opts fileopts;
	byte* filedata;
};

struct tag_entry_serialxml
{
	std::wstring tagname;
	std::map<std::wstring, std::wstring> tagparams;
	unsigned int childs;
	unsigned int mode;
	unsigned int childMode;
};
// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
