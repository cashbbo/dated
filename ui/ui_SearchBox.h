/********************************************************************************
** Form generated from reading UI file 'SearchBox.ui'
**
** Created: Thu 15. Nov 19:52:16 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCHBOX_H
#define UI_SEARCHBOX_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SearchBox
{
public:
    QPlainTextEdit *plainTextEdit;
    QPushButton *btnSearch;
    QGroupBox *groupBox;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QPushButton *btnSearch_2;
    QPlainTextEdit *plainTextEdit_2;
    QGroupBox *groupBox_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;

    void setupUi(QWidget *SearchBox)
    {
        if (SearchBox->objectName().isEmpty())
            SearchBox->setObjectName(QString::fromUtf8("SearchBox"));
        SearchBox->setWindowModality(Qt::ApplicationModal);
        SearchBox->resize(409, 148);
        plainTextEdit = new QPlainTextEdit(SearchBox);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setGeometry(QRect(20, 20, 271, 31));
        btnSearch = new QPushButton(SearchBox);
        btnSearch->setObjectName(QString::fromUtf8("btnSearch"));
        btnSearch->setGeometry(QRect(320, 20, 75, 23));
        groupBox = new QGroupBox(SearchBox);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(20, 60, 121, 71));
        radioButton = new QRadioButton(groupBox);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        radioButton->setGeometry(QRect(20, 20, 82, 17));
        radioButton_2 = new QRadioButton(groupBox);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
        radioButton_2->setGeometry(QRect(20, 40, 82, 17));
        btnSearch_2 = new QPushButton(SearchBox);
        btnSearch_2->setObjectName(QString::fromUtf8("btnSearch_2"));
        btnSearch_2->setGeometry(QRect(300, 0, 75, 23));
        plainTextEdit_2 = new QPlainTextEdit(SearchBox);
        plainTextEdit_2->setObjectName(QString::fromUtf8("plainTextEdit_2"));
        plainTextEdit_2->setGeometry(QRect(0, 0, 271, 31));
        groupBox_2 = new QGroupBox(SearchBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(0, 40, 121, 71));
        radioButton_3 = new QRadioButton(groupBox_2);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
        radioButton_3->setGeometry(QRect(20, 20, 82, 17));
        radioButton_4 = new QRadioButton(groupBox_2);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));
        radioButton_4->setGeometry(QRect(20, 40, 82, 17));

        retranslateUi(SearchBox);

        QMetaObject::connectSlotsByName(SearchBox);
    } // setupUi

    void retranslateUi(QWidget *SearchBox)
    {
        SearchBox->setWindowTitle(QApplication::translate("SearchBox", "Search", 0, QApplication::UnicodeUTF8));
        btnSearch->setText(QApplication::translate("SearchBox", "&Search", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("SearchBox", "GroupBox", 0, QApplication::UnicodeUTF8));
        radioButton->setText(QApplication::translate("SearchBox", "RadioButton", 0, QApplication::UnicodeUTF8));
        radioButton_2->setText(QApplication::translate("SearchBox", "RadioButton", 0, QApplication::UnicodeUTF8));
        btnSearch_2->setText(QApplication::translate("SearchBox", "&Search", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("SearchBox", "GroupBox", 0, QApplication::UnicodeUTF8));
        radioButton_3->setText(QApplication::translate("SearchBox", "RadioButton", 0, QApplication::UnicodeUTF8));
        radioButton_4->setText(QApplication::translate("SearchBox", "RadioButton", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SearchBox: public Ui_SearchBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCHBOX_H
