#ifndef DATWORKER_H
#define DATWORKER_H

#include <QString>
#include <QProcess>

class DatWorker
{
private:
    QProcess process;
    QString datedPath;

private:
    DatWorker() : process(0), datedPath("dated.exe") {}

public:
    static DatWorker* getInstance() { static DatWorker instance; return &instance; }

    void setDatedPath(QString path) { datedPath = path; }

    void unpack(QString file);
    void pack(QString file);

    QProcess* getProcess() { return &process; }
};

#endif // DATWORKER_H
