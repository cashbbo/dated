#include "About.h"
#include "ui_About.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
}

void About::setVersion(QString version)
{
    ui->labelInfo->setText(ui->labelInfo->text().replace("{%version%}", version));
}

About::~About()
{
    delete ui;
}
