#include "Util.h"

#include <QDir>
#include <QFileInfo>

QList<QString> Util::listFiles(QString path)
{
    QList<QString> files;
    QDir dir = QDir(path);
    foreach (QString file, dir.entryList(QDir::Files | QDir::NoSymLinks))
    {
        files.append(file);
    }

    foreach (QString subdir, dir.entryList(QDir::Dirs | QDir::NoSymLinks))
    {
        if (subdir == "." || subdir == "..")
            continue;

        foreach (QString subfile, listFiles(path + "/" + subdir))
            files.append(subdir + "/" + subfile);
    }

    return files;
}

void Util::removeAll(QString path)
{
    QDir dir = QDir(path);

    if (dir.exists())
    {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst))
        {
            if (info.isDir())
            {
                removeAll(info.absoluteFilePath());
            }
            else
            {
                QFile::remove(info.absoluteFilePath());
            }
        }
        dir.rmdir(path);
    }
}
