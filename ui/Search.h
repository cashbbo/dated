#ifndef SEARCH_H
#define SEARCH_H

#include <QDialog>

namespace Ui {
class Search;
}

class Search : public QDialog
{
    Q_OBJECT
    
private:
    // -1 - Up, 1 - Down
    int direction;
    // 0 - Normal, 1 - Regex
    int mode;
    // Search using case insensitive flag?
    bool caseInsensitive;
    // Search whole words?
    bool searchWholeWords;

public:
    explicit Search(QWidget *parent = 0);
    ~Search();

    void setSearchText(QString text);
    
private:
    Ui::Search *ui;

    bool search(QString text, bool lookup);
    int count(QString text, bool suppressWarnings = false);
    bool replace(QString what, QString with, bool suppressWarnings = false);
    void replaceAll(QString what, QString with);

public slots:
    void onCaseChange(bool);

    void onWholeWordChange(bool);

    void onDirChange(bool);

    void onModeChange(bool);

    void onSearch();

    void onCount();

    void onReplace();

    void onReplaceAll();
};

#endif // SEARCH_H
