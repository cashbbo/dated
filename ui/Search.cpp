#include <QTextDocument>
#include <QTextDocumentFragment>
#include <QMessageBox>
#include <QDebug>

#include "MainWindow.h"
#include "Search.h"
#include "ui_Search.h"

Search::Search(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Search),
    direction(1),
    mode(0),
    caseInsensitive(true),
    searchWholeWords(false)
{
    ui->setupUi(this);

    connect(ui->btnSearch, SIGNAL(clicked()), this, SLOT(onSearch()));
    connect(ui->btnCount, SIGNAL(clicked()), this, SLOT(onCount()));
    connect(ui->btnReplace, SIGNAL(clicked()), this, SLOT(onReplace()));
    connect(ui->btnReplaceAll, SIGNAL(clicked()), this, SLOT(onReplaceAll()));

    connect(ui->checkCase, SIGNAL(toggled(bool)), this, SLOT(onCaseChange(bool)));
    connect(ui->checkWholeWord, SIGNAL(toggled(bool)), this, SLOT(onWholeWordChange(bool)));
    connect(ui->radioDirDown, SIGNAL(toggled(bool)), this, SLOT(onDirChange(bool)));
    connect(ui->radioDirUp, SIGNAL(toggled(bool)), this, SLOT(onDirChange(bool)));
    connect(ui->radioModeNormal, SIGNAL(toggled(bool)), this, SLOT(onModeChange(bool)));
    connect(ui->radioModeRegex, SIGNAL(toggled(bool)), this, SLOT(onModeChange(bool)));
}

void Search::setSearchText(QString text)
{
    ui->editSearchText->setText(text);
}

void Search::onCaseChange(bool value)
{
    caseInsensitive = value;
}

void Search::onWholeWordChange(bool value)
{
    searchWholeWords = value;
}

void Search::onDirChange(bool)
{
    direction = (ui->radioDirUp->isChecked()) ? -1 : 1;
}

void Search::onModeChange(bool)
{
    mode = (ui->radioModeNormal->isChecked()) ? 0 : 1;
}

void Search::onSearch()
{
    search(ui->editSearchText->text(), true);
}

void Search::onCount()
{
    count(ui->editSearchText->text());
}

void Search::onReplace()
{
    replace(ui->editSearchText->text(), ui->editReplaceText->text());
}

void Search::onReplaceAll()
{
    replaceAll(ui->editSearchText->text(), ui->editReplaceText->text());
}

bool Search::replace(QString what, QString with, bool suppressWarnings)
{
    QTextEdit* edit = ((MainWindow*)parent())->getTextEdit();

    QTextCursor cursor = edit->textCursor();
    QTextCursor bookmark = QTextCursor(cursor);
    cursor.setPosition(cursor.position() - cursor.selection().toPlainText().size());
    edit->setTextCursor(cursor);

    if (search(what, true))
    {
        int size = edit->textCursor().selection().toPlainText().size();
        int pos = edit->textCursor().position() - size;

        if (pos >= 0 && size > 0)
        {
            edit->setPlainText(edit->toPlainText().replace(pos, size, with));
        }

        search(what, true);
        return true;
    }
    else
    {
        edit->setTextCursor(bookmark);

        if (!suppressWarnings)
        {
            QMessageBox* msg = new QMessageBox(this);
            msg->setText("No occurances found.");
            msg->show();
        }
        return false;
    }
}

void Search::replaceAll(QString what, QString with)
{
    int cnt = count(what, true);
    QTextEdit* edit = ((MainWindow*)parent())->getTextEdit();

    if (searchWholeWords)
        what = "(?!\\w)" + what + "(?!\\w)";

    QRegExp regex = QRegExp(what, caseInsensitive ? Qt::CaseInsensitive : Qt::CaseSensitive);

    edit->setPlainText(edit->toPlainText().replace(regex, with));

    QMessageBox* msg = new QMessageBox(this);
    msg->setText(QString("%1 occurances replaced.").arg(cnt));
    msg->show();
}

int Search::count(QString text, bool suppressWarnings)
{
    if (text.isEmpty())
        return 0;

    int counter = 0;

    QTextEdit* edit = ((MainWindow*)parent())->getTextEdit();
    QTextCursor cursor = edit->textCursor();
    QTextCursor bookmark = QTextCursor(cursor);
    cursor.setPosition(0);
    edit->setTextCursor(cursor);

    while (search(text, false))
    {
        ++counter;
    }

    // Restore old cursor
    edit->setTextCursor(bookmark);

    if (!suppressWarnings)
    {
        QMessageBox* msg = new QMessageBox(this);
        msg->setText(QString("Found %1 occurances").arg(counter));
        msg->show();
    }
    return counter;
}

bool Search::search(QString text, bool lookup)
{
    if (text.isEmpty())
        return false;

    QTextEdit* edit = ((MainWindow*)parent())->getTextEdit();
    QTextDocument* doc = edit->document();

    // Return back to selection size chars
    if (!edit->textCursor().selection().isEmpty())
    {
        edit->textCursor().movePosition(QTextCursor::PreviousCharacter, QTextCursor::MoveAnchor, edit->textCursor().selection().toPlainText().size());
    }

    int flags;
    if (!caseInsensitive)
        flags |= QTextDocument::FindCaseSensitively;

    if (direction == -1)
        flags |= QTextDocument::FindBackward;

    if (searchWholeWords)
        flags |= QTextDocument::FindWholeWords;

    QTextCursor cursor;
    if (mode == 0)
    {
        cursor = doc->find(text, edit->textCursor(), (QTextDocument::FindFlag)flags);
    }
    else if (mode == 1)
    {
        cursor = doc->find(QRegExp(text), edit->textCursor(), (QTextDocument::FindFlag)flags);
    }

    // Found?
    if (!cursor.isNull())
    {
        edit->setTextCursor(cursor);
        return true;
    }
    else if (lookup)
    {
        QTextCursor cursor = edit->textCursor();
        QTextCursor bookmark = QTextCursor(cursor);
        cursor.setPosition(0);
        edit->setTextCursor(cursor);
        bool result = search(text, false);
        if (!result)
            edit->setTextCursor(bookmark);

        return result;
    }

    return false;
}

Search::~Search()
{
    delete ui;
}
