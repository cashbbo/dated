#-------------------------------------------------
#
# Project created by QtCreator 2012-11-12T23:22:26
#
#-------------------------------------------------

QT       += core gui

TARGET = BnS_DatEditor
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    DatWorker.cpp \
    Util.cpp \
    About.cpp \
    Search.cpp

HEADERS  += MainWindow.h \
    DatWorker.h \
    Util.h \
    About.h \
    Config.h \
    Search.h

FORMS    += MainWindow.ui \
    About.ui \
    Search.ui

RESOURCES += \
    main.qrc

RC_FILE = main.rc
