#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/**
 * Main window header.
 *
 * @author Yorie
 */

#include <QMainWindow>
#include <QTextEdit>
#include <QListWidget>
#include <QString>
#include <QList>
#include <QFile>
#include <QComboBox>
#include <QPushButton>
#include <QTextDocument>

#include "Search.h"
#include "About.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QTextEdit* getTextEdit();
    
private:
    Ui::MainWindow *ui;
    QListWidget* fileList;
    QTextEdit* dataEdit;
    QTextEdit* logEdit;
    QComboBox* proto;
    QList<QString> files;
    QFile* editable;
    bool textChanged;
    QString currentFile;
    QString lastDatFileName;
    Search* searchBox;
    About* about;

    QPushButton* btnSave;

    void init();
    void reopen(QString path);
    void closeEvent(QCloseEvent *);

public slots:
    // Open .dat file
    void openDat();

    // Open search box
    void openSearch();

    // Save .dat file
    void saveDat();

    // Dat worker output is ready
    void outputReady();

    // Dat worker finished its work
    void workerFinished();

    // When user selected another protocol version
    void onProtocolVersionChange(int);

    // When user double clicks on file list view
    void onFileListDoubleClick(QModelIndex modelIndex);

    // When user changes opened file data
    void onDataTextChanged();

    // About dialog
    void onAboutClicked();

    // When user wants to save file
    void onSaveClicked();

    // When user wants to change word-wrap mode
    void onWrapModeToggled(bool value);
};

#endif // MAINWINDOW_H
