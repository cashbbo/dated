/********************************************************************************
** Form generated from reading UI file 'Search.ui'
**
** Created: Thu 15. Nov 22:47:29 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCH_H
#define UI_SEARCH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_Search
{
public:
    QPushButton *btnSearch;
    QGroupBox *groupBox;
    QRadioButton *radioModeNormal;
    QRadioButton *radioModeRegex;
    QLineEdit *editSearchText;
    QLineEdit *editReplaceText;
    QGroupBox *groupBox_2;
    QRadioButton *radioDirUp;
    QRadioButton *radioDirDown;
    QPushButton *btnReplace;
    QPushButton *btnReplaceAll;
    QPushButton *btnCount;
    QCheckBox *checkCase;
    QCheckBox *checkWholeWord;

    void setupUi(QDialog *Search)
    {
        if (Search->objectName().isEmpty())
            Search->setObjectName(QString::fromUtf8("Search"));
        Search->setWindowModality(Qt::NonModal);
        Search->resize(416, 187);
        Search->setFocusPolicy(Qt::NoFocus);
        Search->setModal(false);
        btnSearch = new QPushButton(Search);
        btnSearch->setObjectName(QString::fromUtf8("btnSearch"));
        btnSearch->setGeometry(QRect(320, 10, 75, 24));
        groupBox = new QGroupBox(Search);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 100, 161, 71));
        radioModeNormal = new QRadioButton(groupBox);
        radioModeNormal->setObjectName(QString::fromUtf8("radioModeNormal"));
        radioModeNormal->setGeometry(QRect(20, 20, 82, 17));
        radioModeNormal->setChecked(true);
        radioModeRegex = new QRadioButton(groupBox);
        radioModeRegex->setObjectName(QString::fromUtf8("radioModeRegex"));
        radioModeRegex->setGeometry(QRect(20, 40, 121, 17));
        editSearchText = new QLineEdit(Search);
        editSearchText->setObjectName(QString::fromUtf8("editSearchText"));
        editSearchText->setGeometry(QRect(10, 10, 291, 24));
        editReplaceText = new QLineEdit(Search);
        editReplaceText->setObjectName(QString::fromUtf8("editReplaceText"));
        editReplaceText->setGeometry(QRect(10, 50, 291, 24));
        groupBox_2 = new QGroupBox(Search);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(190, 100, 111, 71));
        radioDirUp = new QRadioButton(groupBox_2);
        radioDirUp->setObjectName(QString::fromUtf8("radioDirUp"));
        radioDirUp->setGeometry(QRect(20, 20, 82, 17));
        radioDirUp->setChecked(false);
        radioDirDown = new QRadioButton(groupBox_2);
        radioDirDown->setObjectName(QString::fromUtf8("radioDirDown"));
        radioDirDown->setGeometry(QRect(20, 40, 121, 17));
        radioDirDown->setChecked(true);
        btnReplace = new QPushButton(Search);
        btnReplace->setObjectName(QString::fromUtf8("btnReplace"));
        btnReplace->setGeometry(QRect(320, 90, 75, 24));
        btnReplaceAll = new QPushButton(Search);
        btnReplaceAll->setObjectName(QString::fromUtf8("btnReplaceAll"));
        btnReplaceAll->setGeometry(QRect(320, 130, 75, 24));
        btnCount = new QPushButton(Search);
        btnCount->setObjectName(QString::fromUtf8("btnCount"));
        btnCount->setGeometry(QRect(320, 50, 75, 24));
        checkCase = new QCheckBox(Search);
        checkCase->setObjectName(QString::fromUtf8("checkCase"));
        checkCase->setGeometry(QRect(10, 80, 101, 17));
        checkCase->setChecked(true);
        checkWholeWord = new QCheckBox(Search);
        checkWholeWord->setObjectName(QString::fromUtf8("checkWholeWord"));
        checkWholeWord->setGeometry(QRect(120, 80, 91, 17));
        checkWholeWord->setFocusPolicy(Qt::WheelFocus);
        checkWholeWord->setChecked(false);
        QWidget::setTabOrder(editSearchText, editReplaceText);
        QWidget::setTabOrder(editReplaceText, btnSearch);
        QWidget::setTabOrder(btnSearch, btnCount);
        QWidget::setTabOrder(btnCount, btnReplace);
        QWidget::setTabOrder(btnReplace, btnReplaceAll);
        QWidget::setTabOrder(btnReplaceAll, checkCase);
        QWidget::setTabOrder(checkCase, checkWholeWord);
        QWidget::setTabOrder(checkWholeWord, radioModeNormal);
        QWidget::setTabOrder(radioModeNormal, radioModeRegex);
        QWidget::setTabOrder(radioModeRegex, radioDirUp);
        QWidget::setTabOrder(radioDirUp, radioDirDown);

        retranslateUi(Search);

        QMetaObject::connectSlotsByName(Search);
    } // setupUi

    void retranslateUi(QDialog *Search)
    {
        Search->setWindowTitle(QApplication::translate("Search", "Search", 0, QApplication::UnicodeUTF8));
        btnSearch->setText(QApplication::translate("Search", "&Search", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Search", "Mode", 0, QApplication::UnicodeUTF8));
        radioModeNormal->setText(QApplication::translate("Search", "&Normal", 0, QApplication::UnicodeUTF8));
        radioModeRegex->setText(QApplication::translate("Search", "&Regular Expression", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("Search", "Direction", 0, QApplication::UnicodeUTF8));
        radioDirUp->setText(QApplication::translate("Search", "&Up", 0, QApplication::UnicodeUTF8));
        radioDirDown->setText(QApplication::translate("Search", "&Down", 0, QApplication::UnicodeUTF8));
        btnReplace->setText(QApplication::translate("Search", "R&eplace", 0, QApplication::UnicodeUTF8));
        btnReplaceAll->setText(QApplication::translate("Search", "Replace &All", 0, QApplication::UnicodeUTF8));
        btnCount->setText(QApplication::translate("Search", "&Count", 0, QApplication::UnicodeUTF8));
        checkCase->setText(QApplication::translate("Search", "Case &insensitive", 0, QApplication::UnicodeUTF8));
        checkWholeWord->setText(QApplication::translate("Search", "&Whole words", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Search: public Ui_Search {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCH_H
